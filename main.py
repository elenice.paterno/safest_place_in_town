from app.bootstrap import StartUp
from app.town.town import Town
from app.agent.agent import Agent

if __name__ == '__main__':
    ag = Agent([(0, 1), (0, 0), (1, 0)])
    tw = Town(2)
    sp = StartUp(ag, tw)
    print(sp.start())

