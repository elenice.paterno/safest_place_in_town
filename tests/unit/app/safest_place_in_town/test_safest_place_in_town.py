import unittest
from app.safest_place_in_town.safest_place_in_town import SafestPlacesInTown
from app.agent.agent import Agent
from app.town.town import Town
from app.bootstrap import StartUp
from app.validation.validation import Validations
from unittest.mock import patch, MagicMock


class AdviceTest(unittest.TestCase):
    def test_2x2grid(self):
        ag = Agent([(1, 1)])
        tw = Town(2)
        sp = StartUp(ag, tw)
        self.assertEqual(sp.start(), [(0, 0)])

    def test_size_0(self):
        ag = Agent([(1, 1)])
        tw = Town(0)
        sp = StartUp(ag, tw)
        self.assertEqual(sp.start(), [])

    def test_agents_everywhere(self):
        ag = Agent([(0, 0)])
        tw = Town(1)
        sp = StartUp(ag, tw)
        self.assertEqual(sp.start(), [])

    def test_size(self):
        ag = Agent([(9, 9)])
        tw = Town(1)
        sp = StartUp(ag, tw)
        self.assertEqual(sp.start(), [(0, 0)])

    def test_single_agent(self):
        ag = Agent([(0, 0)])
        tw = Town(2)
        sp = StartUp(ag, tw)
        self.assertEqual(sp.start(), [(1, 1)])

    def test_two_agents(self):
        ag = Agent([(0, 0), (1, 1)])
        tw = Town(3)
        sp = StartUp(ag, tw)
        self.assertEqual(sorted(sp.start()), sorted([(0, 2), (2, 0), (2, 2)]))

    def test_validation_zero_agents_should_be_return_town(self):
        ag = Agent([])
        tw = Town(2)
        vl = Validations(ag, tw)
        self.assertIsInstance(vl, Validations)
        self.assertEqual(vl.validation_zero_agents(), [[[0, 0], [0, 1]], [[1, 0], [1, 1]]])

    def test_calculate_distance_should_be_return_validation_zero_agents_if_not_agents(self):
        ag = Agent([])
        tw = Town(2)
        sp = SafestPlacesInTown(ag, tw)
        vl = Validations(ag, tw)
        sp.calculate_distance_agents_spaces_in_town()
        self.assertIsInstance(sp, SafestPlacesInTown)
        self.assertEqual(vl.validation_zero_agents(), [[[0, 0], [0, 1]], [[1, 0], [1, 1]]])

    @patch('app.town.town')
    def test_deep_copy_in_town(self, deep_copy_mock):
        tw = Town(5)
        deep_copy_mock.deepcopy = MagicMock()
        tw.set_town()
        deep_copy_mock.deepcopy.assert_has_calls(deep_copy_mock, any_order=False)
