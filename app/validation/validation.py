from app.town.town import Town
from app.agent.agent import Agent

class Validations:
    def __init__(self, agents: Agent, town: Town):
        self.town = town
        self.agents = agents

    def validation_zero_town_area(self) -> list:
        if self.town.get_town_area() == 0:
            return []

    def validation_zero_agents(self) -> list:
        if not self.agents.list_position:
            return self.town.set_town()

    def validation_town_full_of_agent(self) -> list:
        count = 0
        town_matrix = self.town.town_matrix
        for x in range(self.town.get_town_area()):
            for y in range(self.town.get_town_area()):
                if town_matrix[x][y] == 0:
                    count += 1
        if count == self.town.get_town_area() * self.town.get_town_area():
            return []
