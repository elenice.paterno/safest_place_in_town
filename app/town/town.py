import copy


class Town:
    def __init__(self, town_area: int):
        self.town_matrix = []
        self.area = town_area

    def get_town_area(self) -> int:
        return self.area

    def set_town(self):
        row = []
        column = []

        for x in range(self.area):
            for y in range(self.area):
                row.append([x, y])
            column.append(row.copy())
            row.clear()

        self.town_matrix = copy.deepcopy(column)
        return self.town_matrix
