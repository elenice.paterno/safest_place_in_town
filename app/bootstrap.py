from app.validation.validation import Validations
from app.town.town import Town
from app.safest_place_in_town.safest_place_in_town import SafestPlacesInTown


class StartUp:
    def __init__(self, ag, tw):
        self.town = tw
        self.agent = ag
        self.validate = Validations(ag, tw)
        self.safest_place = SafestPlacesInTown(ag, tw)

    def start(self):
        if self.validate.validation_zero_town_area():
            return self.validate.validation_zero_town_area()
        if self.validate.validation_zero_agents():
            return self.validate.validation_zero_agents()
        if self.validate.validation_town_full_of_agent():
            return self.validate.validation_town_full_of_agent()
        return self.safest_place.build_town()
