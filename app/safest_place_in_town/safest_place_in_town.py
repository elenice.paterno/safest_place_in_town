from app.agent.agent import Agent
from app.town.town import Town


class SafestPlacesInTown:
    def __init__(self, agents: Agent, town: Town):
        self.agents = agents
        self.town = town
        self.town.set_town()

    def insert_agents_in_town(self):
        for agent in range(len(self.agents.get_position())):
            for x in range(self.town.get_town_area()):
                for y in range(self.town.get_town_area()):
                    if self.town.town_matrix[x][y] == self.agents.get_position()[agent]:
                        self.town.town_matrix[x][y] = [0]

    def insert_blank_spaces_in_town(self):
        for x in range(self.town.get_town_area()):
            for y in range(self.town.get_town_area()):
                if self.town.town_matrix[x][y] != [0]:
                    self.town.town_matrix[x][y] = []

    def calculate_distance_agents_spaces_in_town(self):
        for x in range(self.town.get_town_area()):
            for y in range(self.town.get_town_area()):
                if self.town.town_matrix[x][y] != [0]:
                    for agent in self.agents.get_position():
                        x_agent, y_agent = agent
                        x_town, y_town = x, y
                        distance = abs(x_town - x_agent) + abs(y_town - y_agent)
                        self.town.town_matrix[x][y].append(distance)

                (self.town.town_matrix[x][y]) = min(self.town.town_matrix[x][y])

    def safest_place(self):
        safest_place = []
        town_matrix = self.town.town_matrix
        for x in range(self.town.get_town_area()):
            for y in range(self.town.get_town_area()):
                if town_matrix[x][y] >= 2:
                    safest_place.append((x, y))
        return safest_place

    def build_town(self):
        self.town.set_town()
        self.insert_agents_in_town()
        self.insert_blank_spaces_in_town()
        self.calculate_distance_agents_spaces_in_town()
        return self.safest_place()
